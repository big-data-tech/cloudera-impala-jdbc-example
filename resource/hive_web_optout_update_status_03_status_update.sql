INSERT  
    INTO TABLE cookie_id_change_history 
        SELECT  
                join_tb.id, 
                IF(join_tb.t_new < join_tb.t_old,join_tb.optout,join_tb.optout_new)  optout, 
                IF(join_tb.t_new < join_tb.t_old,join_tb.t_old,join_tb.t_new)  optout_time 
            FROM( 
                SELECT  
                        new.cookie_id  id, 
                        old.optout optout , 
                        old.optout_time t_old, 
                        new.optout_status optout_new,  
                        new.optout_time t_new 
                    FROM  
                        cookie_id_change_history old  
                        RIGHT OUTER JOIN  
                            tmp_web_optout_log new  
                                ON old.cookie_id=new.cookie_id 
            )join_tb