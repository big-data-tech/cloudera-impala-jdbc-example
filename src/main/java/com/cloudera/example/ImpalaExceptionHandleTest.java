package com.cloudera.example;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class ImpalaExceptionHandleTest {
	public static void main(String args[]) {
		SingleConnectionDataSource ds = new SingleConnectionDataSource();
		ds.setDriverClassName("org.apache.hive.jdbc.HiveDriver");
		ds.setUrl("jdbc:hive2://localhost:21076/default;auth=noSasl");

		JdbcTemplate impalaTemplate = new JdbcTemplate(ds);
		impalaTemplate.setIgnoreWarnings(false);

		try {
			@SuppressWarnings("deprecation")
			int count = impalaTemplate
					.queryForInt("select count(*)  from pa_sy_rankings R join pa_sy_uservisits U on U.desturl = R.pageurl");
			System.out.println(count);
		} catch(UncategorizedSQLException e){
			System.out.println("222ERROR:  " + e.getMessage());
			System.out.println("222ERROR:  " + e.getSQLException().getErrorCode());
			System.out.println("222ERROR:  " + e.getSQLException().getNextException().getErrorCode());
			e.printStackTrace();
		}
		catch (Exception e) {
			System.out.println("ERROR:  " + e.getMessage());
			e.printStackTrace();
		}

		ds.destroy();
	}
}
