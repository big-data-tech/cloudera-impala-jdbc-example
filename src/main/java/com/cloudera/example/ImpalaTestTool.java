package com.cloudera.example;


public class ImpalaTestTool {
private static int THREAD_NUMS = 1000;
	
	public static void main(String[] args){
		int num = THREAD_NUMS;
		if(args.length == 1){
			num = Integer.parseInt(args[0]);
		}
		
		for(int i = 0 ; i < num ; i ++){
			SpringJdbcImpalaMultiTest impala = new SpringJdbcImpalaMultiTest("111", 1);
			impala.start();
		}
		
	}
}
