package com.cloudera.example;

import java.sql.SQLException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * 
 * @author long-ta
 * Date : 2014/06/20
 * mvn compile exec:java -Dexec.mainClass="com.cloudera.example.SpringJdbcHive"
 */
public class SpringJdbcHive {
	
	public static final String HIVE_JDBC_PORT = "10000";
	public static String HIVE_HOST = "localhost";
    /* we set hive.exec.dynamic.partition.mode=nonstrict to enable using all dynamic keys for dynamic partitions */
	public static final String HIVE_CONNECTION_URL = 
            "jdbc:hive2://" + HIVE_HOST + ':' + HIVE_JDBC_PORT + "/gmo_pdmp?" +
                    "hive.exec.dynamic.partition.mode=nonstrict;" +
                    "parquet.compression=GZIP;";
    public static final String HIVE_JDBC_DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";
    public static final String HIVE_USER = "hdfs";
    public static final String HIVE_PASSWORD = "";
	
	public static void main(String args[]) {
			startQuery();
		
		
        
	}
	
	
	public static void startQuery(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource(HIVE_CONNECTION_URL, HIVE_USER, HIVE_PASSWORD);
        dataSource.setDriverClassName(HIVE_JDBC_DRIVER_NAME);
        JdbcTemplate hiveTemp = new JdbcTemplate(dataSource);
        
//        int num = hiveTemp.queryForInt("select count(*) from cookie_id_change_history");
        
//        System.out.println("aaa:" + num)
        System.out.println("@START");
//        hiveTemp.execute("insert into table  cookie_id_change_history"
//        		+ " select member_id,sync_time,cookie_id from member_cookie_id");
//        
//        hiveTemp.execute("set hive.stats.autogather=false");
//        
//        hiveTemp.execute("INSERT INTO TABLE cookie_id_change_history "
//        		+ "SELECT join_tb.id, IF(join_tb.t_new < join_tb.t_old,join_tb.optout,join_tb.optout_new) optout, "
//        		+ "IF(join_tb.t_new < join_tb.t_old,join_tb.t_old,join_tb.t_new) optout_time "
//        		+ "FROM( SELECT new.cookie_id id, old.optout optout , old.optout_time t_old, "
//        		+ "new.optout_status optout_new, new.optout_time t_new FROM cookie_id_change_history old "
//        		+ "RIGHT OUTER JOIN tmp_web_optout_log new ON old.cookie_id=new.cookie_id )join_tb");
//        hiveTemp.execute("INSERT INTO TABLE cookie_id_change_history select * from tmp_web_optout_log");
        int num = hiveTemp.queryForInt("select count(*) from cookie_id_change_history");
        System.out.println("aaa:" + num);
        System.out.println("@END");
	}
}
