package com.cloudera.example;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;


public class SpringJdbcImpala {
	  public static void main(String args[]) throws Exception {
		    SingleConnectionDataSource ds = new SingleConnectionDataSource();
		    ds.setDriverClassName("org.apache.hive.jdbc.HiveDriver");
		    ds.setUrl("jdbc:hive2://localhost:21050/gmo_pdmp;auth=noSasl");
		    
		    JdbcTemplate impalaTemplate = new JdbcTemplate(ds);
		    impalaTemplate.setIgnoreWarnings(false);
		    
		    
		    impalaTemplate.execute("CREATE TABLE IF NOT EXISTS tmp_segment_tb (id STRING) STORED AS PARQUET");
		    impalaTemplate.execute("INSERT OVERWRITE tmp_segment_tb select cookie_id FROM external_system_cookie_id");
		    
		    
		    int count = impalaTemplate.queryForInt("select count(*) from tmp_segment_tb");    
		    
		    System.out.println(count);
		    
//		    SqlRowSet res = impalaTemplate.queryForRowSet("select id from tmp_segment_tb");
		    
		    impalaTemplate.queryForObject("select * from tmp_segment_tb", new RowMapper<ResultSet>(){

				@Override
				public ResultSet mapRow(ResultSet rs, int rowNum)
						throws SQLException {
					System.out.println("--------" + rs.getString("id"));
					while(rs.next()){
				    	System.out.println("--------" + rs.getString("id"));
				    }
					return rs;
				}
		    	
		    });
		    
		    ds.destroy();
		  }
}
