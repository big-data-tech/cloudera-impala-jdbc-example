package com.cloudera.example;

import java.io.IOException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

public class SpringJdbcImpalaMultiTest extends Thread {
	private Thread t;
	private String threadName;
	private int rows;

	SpringJdbcImpalaMultiTest(String name, int num) {
		threadName = name;
		System.out.println("Creating bb " + threadName);
		rows = num;
	}

	public void run() {
		long start = System.currentTimeMillis();

		SingleConnectionDataSource ds = null;
		try {
			ds = new SingleConnectionDataSource();
			ds.setDriverClassName("org.apache.hive.jdbc.HiveDriver");
//			ds.setUrl("jdbc:hive2://localhost:21076/default;auth=noSasl");
			ds.setUrl("jdbc:hive2://10.112.21.76:21050/default;auth=noSasl");

			JdbcTemplate impalaTemplate = new JdbcTemplate(ds);
//			int nums = impalaTemplate.queryForInt("select count(*) from pa_sy_rankings");
			int nums = impalaTemplate.queryForInt("SELECT COUNT(*) from pa_sy_uservisits_sml U join pa_sy_rankings_sml R on U.desturl = R.pageurl"); 
//			System.out.println("Nums = "+ nums );
			long end = System.currentTimeMillis();
			System.out.println("Time: " + (end - start) / 100);
		} catch (DataAccessException e) {
			e.printStackTrace();
		} finally {
			ds.destroy();
		}
	}

	public void start() {
		System.out.println("Starting " + threadName);
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}
}
