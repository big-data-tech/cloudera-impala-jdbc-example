package com.cloudera.example;

import java.util.Properties;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

/**
 * @author long-ta
 * Date : 2014/06/23
 *
 * mvn compile exec:java -Dexec.mainClass="com.cloudera.example.TestImpalaWithHbaseJdbc"
 */
public class TestImpalaWithHbaseJdbc {
	public static void main(String args[]) throws Exception {
		SingleConnectionDataSource ds = new SingleConnectionDataSource();
	    ds.setDriverClassName("org.apache.hive.jdbc.HiveDriver");
	    ds.setUrl("jdbc:hive2://localhost:21050/gmo_pdmp;auth=noSasl");
	    
	    ds.setConnectionProperties(new Properties());
	    
	    
	    JdbcTemplate impalaTemplate = new JdbcTemplate(ds);
	    
//	    String query = ResourceUtils.getResourceAsString("/resource/hive_web_optout_update_status_03_status_update.sql");
	    
	    
	    impalaTemplate.execute("refresh cookie_id_change_history");
	    impalaTemplate.execute("refresh tmp_web_optout_log");
	    impalaTemplate.execute("INSERT INTO TABLE cookie_id_change_history SELECT join_tb.id, IF(join_tb.t_new < join_tb.t_old,join_tb.optout,join_tb.optout_new) optout, IF(join_tb.t_new < join_tb.t_old,join_tb.t_old,join_tb.t_new) optout_time FROM( SELECT new.cookie_id id, old.optout optout , old.optout_time t_old, new.optout_status optout_new, new.optout_time t_new FROM cookie_id_change_history old RIGHT OUTER JOIN tmp_web_optout_log new ON old.cookie_id=new.cookie_id )join_tb");
	}
}
